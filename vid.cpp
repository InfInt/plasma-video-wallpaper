#define __STDC_CONSTANT_MACROS 1
#include <QPainter>
#include <QTimer>
#include <QThread>
#include <qdatetime.h>
#include <KDirSelectDialog>
#include <KDirWatch>
#include <KFileDialog>
#include <KRandom>
#include <KStandardDirs>
#include <KIO/Job>
#include <KNS/Engine>

#include "vid.h"


Blaa::Blaa(QObject* parent, Tutorial1* tis) : QThread(parent), is(tis)
{
    time2die = false;
    clock.start();  // would start() in main loop if way to test if its started and save some small cycles
  timedebt = 0;
  last_pts = 0;
  last_delay = 0;
}
Tutorial1::Tutorial1(QObject *parent, const QVariantList &args)
        : Plasma::Wallpaper(parent, args), blaa(parent, this) //, img(NULL)
{
  currvid = -1;
  pCodec = NULL;
  pFrame = NULL;
  pFrameRGB = NULL;
  buffer = NULL;
  pCodecCtx = NULL;
  pFormatCtx = NULL;
  frameFinished = 0;
  numBytes = 0;
  video_index = 0;
  mutex = new QMutex;
  confwid = NULL;
  m_dialog = NULL;
//  f = fopen("/home/genjix/out", "a");
}
Tutorial1::~Tutorial1()
{
  blaa.time2die = true;
  blaa.wait();
  DeInit();
  delete mutex;
}

static uint64_t global_video_pkt_pts = AV_NOPTS_VALUE;

/* These are called whenever we allocate a frame
 * buffer. We use this to store the global_pts in
 * a frame at the time it is allocated.
 */
int our_get_buffer(struct AVCodecContext *c, AVFrame *pic) {
  int ret = avcodec_default_get_buffer(c, pic);
  uint64_t *pts = new uint64_t;
  *pts = global_video_pkt_pts;
  pic->opaque = pts;
  return ret;
}
void our_release_buffer(struct AVCodecContext *c, AVFrame *pic) {
  if(pic) delete reinterpret_cast<uint64_t*>(pic->opaque);
  avcodec_default_release_buffer(c, pic);
}

void Tutorial1::Init()
{
  if (currvid >= static_cast<int>(playlist.size()))
  {
    if (playlist.size() == 0)
      currvid = -1;
    else
      currvid = 0;
  }
  if (currvid < 0)
    return;

  // Register all formats and codecs
  av_register_all();

  // Open video file
  if(av_open_input_file(&pFormatCtx, playlist.at(currvid).toLocal8Bit().constData(), NULL, 0, NULL)!=0)
    return; // Couldn't open file

  // Retrieve stream information
  if(av_find_stream_info(pFormatCtx)<0)
    return; // Couldn't find stream information

  // Dump information about file onto standard error
  //dump_format(pFormatCtx, 0, argv[1], 0);

  // Find the first video stream
  video_index=-1;
  for(size_t i=0; i <pFormatCtx->nb_streams; i++)
  {
    if(pFormatCtx->streams[i]->codec->codec_type==CODEC_TYPE_VIDEO) {
      video_index=i;
      break;
    }
  }
  if(video_index==-1)
    return; // Didn't find a video stream

  // Get a pointer to the codec context for the video stream
  video_st = pFormatCtx->streams[video_index];
  pCodecCtx=pFormatCtx->streams[video_index]->codec;
  pCodecCtx->get_buffer = our_get_buffer;
  pCodecCtx->release_buffer = our_release_buffer;

  // Find the decoder for the video stream
  pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
  if(pCodec==NULL) {
    fprintf(stderr, "Unsupported codec!\n");
    return; // Codec not found
  }
  // Open codec
  if(avcodec_open(pCodecCtx, pCodec)<0)
    return; // Could not open codec

  // Allocate video frame
  pFrame=avcodec_alloc_frame();

  mutex->lock();
  // Allocate an AVFrame structure
  pFrameRGB=avcodec_alloc_frame();
  if(pFrameRGB==NULL)
    return;

  // Determine required buffer size and allocate buffer
  numBytes=avpicture_get_size(PIX_FMT_RGB32, pCodecCtx->width,
                  pCodecCtx->height);
  buffer=(uint8_t *)av_malloc(numBytes*sizeof(uint8_t));

  // Assign appropriate parts of buffer to image planes in pFrameRGB
  // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  // of AVPicture
  avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB32,
         pCodecCtx->width, pCodecCtx->height);
  mutex->unlock();
  //clock.start();
}
void Tutorial1::DeInit()
{
  mutex->lock();
  //delete img;

  if (pFormatCtx)
  {
    // Free the RGB image
    av_free(buffer);
    av_free(pFrameRGB);

    // Free the YUV frame
    av_free(pFrame);

    // Close the codec
    avcodec_close(pCodecCtx);

    // Close the video file
    av_close_input_file(pFormatCtx);
  }

  //img = NULL;
  pCodec = NULL;
  pFrame = NULL;
  pFrameRGB = NULL;
  buffer = NULL;
  pCodecCtx = NULL;
  pFormatCtx = NULL;
  frameFinished = 0;
  numBytes = 0;
  video_index = 0;
  //time = 0;
//  f = NULL;
  mutex->unlock();
}

/*void Blaa::SaveFrame(AVFrame *pFrame, int width, int height)
{
  is->mutex->lock();
    if (!is->img)
    {
      is->img = new QImage(QSize(is->pCodecCtx->width, is->pCodecCtx->height), QImage::Format_RGB32);
    }
  uint8_t *data = is->img->bits();
    for (int y = 0; y < height; y++)
    {
      uint8_t* line = pFrame->data[0] + y*pFrame->linesize[0];
      //memcpy(data, line, 4*width);
      //data += 4*width;
      for (int x = 0; x < width; x++)
      {
        char r = line[x*4], g = line[x*4 + 1], b = line[x*4 + 2];
        data[0] = r;
        data[1] = g;
        data[2] = b;
        data[3] = line[x*4 + 3];
        data += 4;
      }
    }
  is->mutex->unlock();
}*/

void Tutorial1::run()
{
}

void Tutorial1::updator()
{
  emit(update(boundingRect()));
}

void Tutorial1::paint(QPainter *painter, const QRectF&)
{
  if (pFrameRGB)
  {
    mutex->lock();
    /*for (int y = 0; y < pCodecCtx->height; y++)
    {
      const uchar* line = pFrameRGB->data[0] + y*pFrame->linesize[0];
      QImage imag(line, pCodecCtx->width, 1, QImage::Format_RGB32);
      QRectF bounding(boundingRect());
      qreal ypos = y * bounding.height() / float(pCodecCtx->height);
      QRectF dest(0, ypos, bounding.width(), 1);
      painter->drawImage(dest, imag);
    }*/
    const QImage imag(pFrameRGB->data[0], pCodecCtx->width, pCodecCtx->height, pFrameRGB->linesize[0], QImage::Format_RGB32);
    painter->drawImage(boundingRect(), imag);
    mutex->unlock();
  }
    else
    {
      painter->setBrush(Qt::black);
      painter->drawRect(boundingRect());
    }
}

void Tutorial1::init(const KConfigGroup &config)
{
  QStringList strl = config.readEntry("slidepaths", QStringList());
  currvid = -1;
  playlist.clear();
  playlist = strl;
  //currvid = -1;
  if (!blaa.isRunning())
    blaa.start();
}
void Tutorial1::save(KConfigGroup &config)
{
  config.writeEntry("slidepaths", playlist);
}

void Tutorial1::configWidgetDestroyed()
{
    confwid = 0;
}
QWidget *Tutorial1::createConfigurationInterface(QWidget *parent)
{
  confwid = new QWidget(parent);
    connect(confwid, SIGNAL(destroyed(QObject*)), this, SLOT(configWidgetDestroyed()));
  m_uiSlideshow.setupUi(confwid);
  for (size_t i = 0; i < playlist.size(); i++)
    m_uiSlideshow.m_dirlist->addItem(playlist.at(i));
        m_uiSlideshow.m_dirlist->setCurrentRow(0);
        m_uiSlideshow.m_addDir->setIcon(KIcon("list-add"));
        connect(m_uiSlideshow.m_addDir, SIGNAL(clicked()), this, SLOT(slotAddVideo()));
        m_uiSlideshow.m_removeDir->setIcon(KIcon("list-remove"));
        connect(m_uiSlideshow.m_removeDir, SIGNAL(clicked()), this, SLOT(slotRemoveVideo()));

  connect(this, SIGNAL(settingsChanged(bool)), parent, SLOT(settingsChanged(bool)));
  return confwid;
}
void Tutorial1::slotAddVideo()
{
    if (!m_dialog) {
        m_dialog = new KFileDialog(KUrl(), "*.avi *.mkv *.ogg *.mpg *.mpeg *.ogv *.mp4 *.ogm *.asf *.flv *.mp4 *.wmv", confwid);
        m_dialog->setOperationMode(KFileDialog::Opening);
        m_dialog->setInlinePreviewShown(true);
        m_dialog->setCaption(i18n("Select Wallpaper Image File"));
        m_dialog->setModal(false);

        connect(m_dialog, SIGNAL(okClicked()), this, SLOT(wallpaperBrowseCompleted()));
        connect(m_dialog, SIGNAL(destroyed(QObject*)), this, SLOT(fileDialogFinished()));
    }

    m_dialog->show();
    m_dialog->raise();
    m_dialog->activateWindow();
}
void Tutorial1::fileDialogFinished()
{
    m_dialog = 0;
}

void Tutorial1::wallpaperBrowseCompleted()
{
    const QString wallpaper = m_dialog->selectedFile();

    if (wallpaper.isEmpty()) {
        return;
    }

  currvid = playlist.size();
  playlist << wallpaper;
  m_uiSlideshow.m_dirlist->addItem(wallpaper);
  m_uiSlideshow.m_removeDir->setEnabled(true);
    emit settingsChanged(true);
}

void Tutorial1::slotRemoveVideo()
{
    int row = m_uiSlideshow.m_dirlist->currentRow();
    if (row != -1) {
        m_uiSlideshow.m_dirlist->takeItem(row);
        updatePlaylist();
        //startSlideshow();
        if (currvid == row)
          currvid = -1;
    }
    emit settingsChanged(true);
}
void Tutorial1::updatePlaylist()
{
    playlist.clear();
    const int dirCount = m_uiSlideshow.m_dirlist->count();
    for (int i = 0; i < dirCount; ++i) {
        playlist << m_uiSlideshow.m_dirlist->item(i)->text();
    }

    m_uiSlideshow.m_removeDir->setEnabled(m_uiSlideshow.m_dirlist->currentRow() != -1);
}

double Blaa::synchronize_video(double pts)
{
  double frame_delay;

  if(pts != 0) {
    /* if we have pts, set video clock to it */
    video_clock = pts;
  } else {
    /* if we aren't given a pts, set it to the clock */
    pts = video_clock;
  }
  /* update the video clock */
  frame_delay = av_q2d(is->video_st->codec->time_base);
  /* if we are repeating a frame, adjust clock accordingly */
  frame_delay += is->pFrame->repeat_pict * (frame_delay * 0.5);
  video_clock += frame_delay;
  return pts;
}
void Blaa::run()
{
  /*int64_t seek_target = time * AV_TIME_BASE/1000;
  if(video_index>=0){
    seek_target= av_rescale_q(seek_target, AV_TIME_BASE_Q,
                      pFormatCtx->streams[video_index]->time_base);
  }
  if(av_seek_frame(pFormatCtx, video_index,
                    seek_target, 0) < 0) {
    fprintf(stderr, "%s: error while seeking\n",
            pFormatCtx->filename);
  }*/

    // Read frames and save first five frames to disk
    while (!time2die) {

        is->currvid++;
        is->Init();
        clock.restart();
        while (is->currvid >= 0 && !time2die &&
            is->pFormatCtx && av_read_frame(is->pFormatCtx, &(is->packet))>=0) {
            // Is this a packet from the video stream?
            if(is->packet.stream_index==is->video_index) {
                // Decode video frame
                avcodec_decode_video(is->pCodecCtx, is->pFrame, &(is->frameFinished),
                    is->packet.data, is->packet.size);
                double pts = 0;
                if(is->packet.dts == AV_NOPTS_VALUE
                    && is->pFrame->opaque && *(uint64_t*)is->pFrame->opaque != AV_NOPTS_VALUE) {
                    pts = *(uint64_t *)is->pFrame->opaque;
                } else if(is->packet.dts != AV_NOPTS_VALUE) {
                    pts = is->packet.dts;
                } else {
                    pts = 0;
                }
                pts *= av_q2d(is->video_st->time_base);

                // Did we get a video frame?
                if(is->frameFinished) {
                    SwsContext *img_convert_ctx =
                        sws_getContext(is->pCodecCtx->width, is->pCodecCtx->height,
                            is->pCodecCtx->pix_fmt,
                            is->pCodecCtx->width, is->pCodecCtx->height,
                            PIX_FMT_RGB32,
                            SWS_BILINEAR|SWS_CPU_CAPS_MMX, NULL, NULL, NULL);
                            //SWS_BILINEAR, NULL, NULL, NULL);

                    is->mutex->lock();
                    sws_scale(img_convert_ctx, is->pFrame->data,
                        is->pFrame->linesize, 0, is->pCodecCtx->height,
                        is->pFrameRGB->data, is->pFrameRGB->linesize);

                    sws_freeContext(img_convert_ctx);
                    is->mutex->unlock();

                    pts = synchronize_video(pts);
                    //fprintf(is->f, "%f\n", pts);
                    //fflush(is->f);
                    int delay = (pts - last_pts)*1000;
                    if (delay <= 0 || delay >= 1000) {
                        delay = last_delay;
                    }
                    last_delay = delay;
                    last_pts = pts;

                    int elapsed = clock.restart();  // milliseconds
                    if (elapsed > delay) {
                        timedebt = 0;
                    }
                    delay -= elapsed + timedebt;
                    timedebt = 0;
                    if (delay < 0) {
                        timedebt = -delay;
                        is->updator();
                    }
                    else if (!time2die) {
                        is->updator();
                        QThread::msleep(delay);
                    }
                }
            }

            // Free the packet that was allocated by av_read_frame
            av_free_packet(&(is->packet));
        }   // decoding loop

        if (!time2die)
        {
            last_pts = 0;
            last_delay = 0;
            timedebt = 0;
            is->DeInit();
            if (is->playlist.empty())
                sleep(1);
        }
    }   // main thread loop
}

#include "vid.moc"
