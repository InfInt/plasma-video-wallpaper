#ifndef PLASMA_WALLPAPER_TUTORIAL1
#define PLASMA_WALLPAPER_TUTORIAL1

#include <Plasma/Wallpaper>
#include <stdint.h>
#include <QMutex>
#include <QWaitCondition>
#include <keditlistbox.h>

extern "C"
{
  #include <avcodec.h>
  #include <avformat.h>
  #include <swscale.h>
}

#include "ui_videoconfig.h"

class Tutorial1;

class Blaa : public QThread
{
  Q_OBJECT
  public:
    Blaa(QObject* parent, Tutorial1* tis);
    //void SaveFrame(AVFrame *pFrame, int width, int height);
    void run();
  Tutorial1* is;
  bool time2die;
  QTime clock;
  int timedebt;
  double last_pts;
  int last_delay;
  double video_clock;   // dont ask me what this is for
    double synchronize_video(double pts);
};

class Tutorial1 : public Plasma::Wallpaper
{
    Q_OBJECT
    public:
        Tutorial1(QObject* parent, const QVariantList& args);
        ~Tutorial1();
        void init(const KConfigGroup &config);
        void save(KConfigGroup &config);
        void Init();
        void DeInit();
        void paint(QPainter* painter, const QRectF& exposedRect);
        void run();
    public Q_SLOTS:
        void updator();
    public:

  QWidget *createConfigurationInterface(QWidget *parent);
void configWidgetDestroyed();

  QWidget* confwid;
  Ui::SlideshowConfig m_uiSlideshow;
  KFileDialog *m_dialog;

//        QImage *img;
  AVCodec         *pCodec;
  AVFrame         *pFrame;
  AVFrame         *pFrameRGB;
  AVStream        *video_st;
  uint8_t         *buffer;
  AVCodecContext  *pCodecCtx;
  AVFormatContext *pFormatCtx;
  AVPacket        packet;
  int             frameFinished;
  int             numBytes;
  int video_index;
  //QTimer timer;
  FILE *f;
  Blaa blaa;
  QMutex* mutex;
  //std::queue<QImage> images;
  // use std::string cos of easier type conversion
  QStringList playlist;
  int currvid;
  void updatePlaylist();


  protected slots:
    void slotAddVideo();
    void slotRemoveVideo();
        void fileDialogFinished();
        void wallpaperBrowseCompleted();
  signals:
        void settingsChanged(bool);
};

K_EXPORT_PLASMA_WALLPAPER(tut, Tutorial1)

#endif
