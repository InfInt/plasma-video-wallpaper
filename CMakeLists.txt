include(FeatureSummary)
cmake_minimum_required(VERSION 3.14)
project(plasma-wallpaper-video)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")

find_package(libavformat)
set_package_properties(libavformat PROPERTIES TYPE REQUIRED PURPOSE "")
find_package(libavcodec)
set_package_properties(libavcodec PROPERTIES TYPE REQUIRED PURPOSE "")
find_package(libswscale)
set_package_properties(libswscale PROPERTIES TYPE REQUIRED PURPOSE "")
find_package(libavutil)
set_package_properties(libavutil PROPERTIES TYPE REQUIRED PURPOSE "")
find_package(KDE5)
set_package_properties(KDE5 PROPERTIES TYPE REQUIRED PURPOSE "")
find_package(KDE5Workspace)
set_package_properties(KDE5Workspace PROPERTIES TYPE REQUIRED PURPOSE "")
include(KDE5Defaults)

add_definitions(${KDE5_DEFINITIONS})
include_directories(${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR} ${KDE5_INCLUDES} ${libavformat_INCLUDES} ${libavcodec_INCLUDES} ${libswscale_INCLUDES} ${libavutil_INCLUDES})

set(video_SRCS
    vid.cpp
)
kde5_add_ui_files(video_SRCS videoconfig.ui)

kde5_add_plugin(wallpapervideo ${video_SRCS})
target_link_libraries(wallpapervideo ${KDE5_PLASMA_LIBS} ${KDE5_KIO_LIBS} ${KDE5_KFILE_LIBS} ${libavformat_LIBRARIES} ${libavcodec_LIBRARIES} ${libswscale_LIBRARIES} ${libavutil_LIBRARIES})

install(TARGETS wallpapervideo DESTINATION ${PLUGIN_INSTALL_DIR})
install(FILES video.desktop DESTINATION ${SERVICES_INSTALL_DIR})

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
