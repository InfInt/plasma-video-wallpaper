## -----------------------------------------------------------------------------
## Check for the header files

find_path (libavformat_INCLUDES avformat.h
  PATHS /usr/local/include/ /usr/include
  PATH_SUFFIXES libavformat/ ffmpeg/
  )

## -----------------------------------------------------------------------------
## Check for the library

find_library (libavformat_LIBRARIES avformat
  PATHS /usr/local/lib /usr/lib /lib
  )

## -----------------------------------------------------------------------------
## Actions taken when all components have been found

if (libavformat_INCLUDES AND libavformat_LIBRARIES)
  set (HAVE_libavformat TRUE)
else (libavformat_INCLUDES AND libavformat_LIBRARIES)
  if (NOT libavformat_FIND_QUIETLY)
    if (NOT libavformat_INCLUDES)
      message (STATUS "Unable to find libavformat header files!")
    endif (NOT libavformat_INCLUDES)
    if (NOT libavformat_LIBRARIES)
      message (STATUS "Unable to find libavformat library files!")
    endif (NOT libavformat_LIBRARIES)
  endif (NOT libavformat_FIND_QUIETLY)
endif (libavformat_INCLUDES AND libavformat_LIBRARIES)

if (HAVE_libavformat)
  if (NOT libavformat_FIND_QUIETLY)
    message (STATUS "Found components for libavformat")
    message (STATUS "libavformat_INCLUDES = ${libavformat_INCLUDES}")
    message (STATUS "libavformat_LIBRARIES     = ${libavformat_LIBRARIES}")
  endif (NOT libavformat_FIND_QUIETLY)
else (HAVE_libavformat)
  if (libavformat_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find libavformat!")
  endif (libavformat_FIND_REQUIRED)
endif (HAVE_libavformat)

mark_as_advanced (
  HAVE_libavformat
  libavformat_LIBRARIES
  libavformat_INCLUDES
  )
