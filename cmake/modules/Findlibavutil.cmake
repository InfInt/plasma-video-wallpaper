## -----------------------------------------------------------------------------
## Check for the header files

find_path (libavutil_INCLUDES avutil.h
  PATHS /usr/local/include/ /usr/include
  PATH_SUFFIXES libavutil/ ffmpeg/
  )

## -----------------------------------------------------------------------------
## Check for the library

find_library (libavutil_LIBRARIES avutil
  PATHS /usr/local/lib /usr/lib /lib
  )

## -----------------------------------------------------------------------------
## Actions taken when all components have been found

if (libavutil_INCLUDES AND libavutil_LIBRARIES)
  set (HAVE_libavutil TRUE)
else (libavutil_INCLUDES AND libavutil_LIBRARIES)
  if (NOT libavutil_FIND_QUIETLY)
    if (NOT libavutil_INCLUDES)
      message (STATUS "Unable to find libavutil header files!")
    endif (NOT libavutil_INCLUDES)
    if (NOT libavutil_LIBRARIES)
      message (STATUS "Unable to find libavutil library files!")
    endif (NOT libavutil_LIBRARIES)
  endif (NOT libavutil_FIND_QUIETLY)
endif (libavutil_INCLUDES AND libavutil_LIBRARIES)

if (HAVE_libavutil)
  if (NOT libavutil_FIND_QUIETLY)
    message (STATUS "Found components for libavutil")
    message (STATUS "libavutil_INCLUDES = ${libavutil_INCLUDES}")
    message (STATUS "libavutil_LIBRARIES     = ${libavutil_LIBRARIES}")
  endif (NOT libavutil_FIND_QUIETLY)
else (HAVE_libavutil)
  if (libavutil_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find libavutil!")
  endif (libavutil_FIND_REQUIRED)
endif (HAVE_libavutil)

mark_as_advanced (
  HAVE_libavutil
  libavutil_LIBRARIES
  libavutil_INCLUDES
  )
