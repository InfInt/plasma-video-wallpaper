## -----------------------------------------------------------------------------
## Check for the header files

find_path (libavcodec_INCLUDES avcodec.h
  PATHS /usr/local/include/ /usr/include
  PATH_SUFFIXES libavcodec/ ffmpeg/
  )

## -----------------------------------------------------------------------------
## Check for the library

find_library (libavcodec_LIBRARIES avcodec
  PATHS /usr/local/lib /usr/lib /lib
  )

## -----------------------------------------------------------------------------
## Actions taken when all components have been found

if (libavcodec_INCLUDES AND libavcodec_LIBRARIES)
  set (HAVE_libavcodec TRUE)
else (libavcodec_INCLUDES AND libavcodec_LIBRARIES)
  if (NOT libavcodec_FIND_QUIETLY)
    if (NOT libavcodec_INCLUDES)
      message (STATUS "Unable to find libavcodec header files!")
    endif (NOT libavcodec_INCLUDES)
    if (NOT libavcodec_LIBRARIES)
      message (STATUS "Unable to find libavcodec library files!")
    endif (NOT libavcodec_LIBRARIES)
  endif (NOT libavcodec_FIND_QUIETLY)
endif (libavcodec_INCLUDES AND libavcodec_LIBRARIES)

if (HAVE_libavcodec)
  if (NOT libavcodec_FIND_QUIETLY)
    message (STATUS "Found components for libavcodec")
    message (STATUS "libavcodec_INCLUDES = ${libavcodec_INCLUDES}")
    message (STATUS "libavcodec_LIBRARIES     = ${libavcodec_LIBRARIES}")
  endif (NOT libavcodec_FIND_QUIETLY)
else (HAVE_libavcodec)
  if (libavcodec_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find libavcodec!")
  endif (libavcodec_FIND_REQUIRED)
endif (HAVE_libavcodec)

mark_as_advanced (
  HAVE_libavcodec
  libavcodec_LIBRARIES
  libavcodec_INCLUDES
  )
