## -----------------------------------------------------------------------------
## Check for the header files

find_path (libswscale_INCLUDES swscale.h
  PATHS /usr/local/include/ /usr/include
  PATH_SUFFIXES libswscale/ ffmpeg/
  )

## -----------------------------------------------------------------------------
## Check for the library

find_library (libswscale_LIBRARIES swscale
  PATHS /usr/local/lib /usr/lib /lib
  )

## -----------------------------------------------------------------------------
## Actions taken when all components have been found

if (libswscale_INCLUDES AND libswscale_LIBRARIES)
  set (HAVE_libswscale TRUE)
else (libswscale_INCLUDES AND libswscale_LIBRARIES)
  if (NOT libswscale_FIND_QUIETLY)
    if (NOT libswscale_INCLUDES)
      message (STATUS "Unable to find libswscale header files!")
    endif (NOT libswscale_INCLUDES)
    if (NOT libswscale_LIBRARIES)
      message (STATUS "Unable to find libswscale library files!")
    endif (NOT libswscale_LIBRARIES)
  endif (NOT libswscale_FIND_QUIETLY)
endif (libswscale_INCLUDES AND libswscale_LIBRARIES)

if (HAVE_libswscale)
  if (NOT libswscale_FIND_QUIETLY)
    message (STATUS "Found components for libswscale")
    message (STATUS "libswscale_INCLUDES = ${libswscale_INCLUDES}")
    message (STATUS "libswscale_LIBRARIES     = ${libswscale_LIBRARIES}")
  endif (NOT libswscale_FIND_QUIETLY)
else (HAVE_libswscale)
  if (libswscale_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find libswscale!")
  endif (libswscale_FIND_REQUIRED)
endif (HAVE_libswscale)

mark_as_advanced (
  HAVE_libswscale
  libswscale_LIBRARIES
  libswscale_INCLUDES
  )
